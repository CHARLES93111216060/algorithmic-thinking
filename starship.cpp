#include <stdio.h>

int main (){
	float distance, time, lightSpeed = 299792.458;
	printf("\n Welcome to this NASA'S program to know the starship travel time \n");
	printf ("\n Write the distance that you want to travel in kilometers by second: \t");
	scanf("%f", &distance);
	time = distance/lightSpeed;
	printf("\n The starship trek time is %0.2f, traveling %0.2f kilometers with light speed %f kilometers by second\n", time, distance, lightSpeed );	
	return 0;
}
