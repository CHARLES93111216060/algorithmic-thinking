#include <stdlib.h>
#include <iostream>
using namespace std;

int main(void){
	float numero, factorial=1;
	int seleccion, i=1;
	cout<<"\n En este programa vamos a hallar el factorial de un numero\n\n";
	cout<<"\n Desea calcular el factorial de algún numero positivo\n 1. Si\n 2. No\n\n";
	cin>> seleccion;			
	if (seleccion ==1){
		cout<<"\n Ingrese un numero para hallar su factorial:\t";
		cin>>numero;
		
		for (i >=1; i <= numero; i++){			
			factorial = factorial*i;				
		}
		cout<<"\n El resultado del factorial del numero " << numero << " es :" << factorial <<"\n\n";		
	}
	else{		
		cout<<"\n Programa finalizado";	
	}	
}
	
